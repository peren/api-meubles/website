<!--
SPDX-License-Identifier: MIT

This file is licensed under an MIT License.
Author: PEReN 2022
-->
# Mentions légales

## Identification de l'éditeur

<p style="text-indent: 0; padding-left: 1.5rem;">Pôle d'Expertise de la Régulation Numérique<br>
120, rue de Bercy<br>
Bâtiment Necker<br>
Télédoc 767<br>
75572 PARIS CEDEX 12</p>

La conception éditoriale, le suivi, la maintenance technique et les mises à jour du site internet peren.gouv.fr sont assurés par le PEReN.

Le PEReN est un service à compétence nationale placé sous l'autorité conjointe des ministres chargés de l'économie, de la communication et du numérique et administrativement rattaché à la Direction Générale des Entreprises, en application du [décret n°2020-1102](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000042297154/) du 31 août 2020.

## Liens hypertextes

Tout site public ou privé est autorisé à établir, sans autorisation préalable, un lien vers les informations diffusées par le Pôle d'Expertise de la Régulation Numérique (PEReN). En revanche, les pages du portail ne doivent pas être imbriquées à l’intérieur des pages d’un autre site.

L’autorisation de mise en place d’un lien est valable pour tout support, à l’exception de ceux diffusant des informations à caractère polémique, pornographique, xénophobe ou pouvant, dans une plus large mesure porter atteinte à la sensibilité du plus grand nombre.

# Données personnelles

## Traitement des données personnelles

La Direction générale des entreprises (DGE) et les communes partenaires (Bordeaux, Nice, Strasbourg, Lyon et La Rochelle) de l’expérimentation s’engagent à ce que la collecte et le traitement de vos données, effectués par transmission entre les intermédiaires de location de meublés volontaires (Booking, Airbnb, Leboncoin, Expedia et CléVacances) et les communes précitées, soient conformes au règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016, dit « règlement général sur la protection des données » (RGPD), relatif à la protection des personnes physiques à l’égard du traitement des données à caractère personnel et à la libre circulation de ces données, et abrogeant la directive 95/46/CE, et à la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés.  

Les données personnelles recueillies dans le cadre de l’expérimentation sont traitées selon des protocoles sécurisés et permettent aux communes et à leur prestataire (le Pôle d’Expertise de la Régulation Numérique - PEReN) de gérer les données reçues.

## Objet du traitement de données

### Finalités:

Le traitement a pour finalité de mettre à la disposition de la commune partenaire ayant mis en place la procédure d’enregistrement de vérifier au travers d’une plateforme que les résidences principales ne sont pas louées plus de 120 jours par an. L’outil numérique, développé par l’Etat, permet la réalisation de ce contrôle sur la base des données transmises par les intermédiaires de location de meublés et cela de manière simple, rapide et harmonisée.  

Catégories de données à caractère personnel
- **Données d’identification** : nom du loueur, numéro de déclaration du meublé ; le fait que ce meublé constitue ou non la résidence principale du loueur au sens de l'article 2 de la loi n° 89-462 du 6 juillet 1989
- **Données d’ordre économique et financier** : nombre de jours de location du meublé en 2020 et en 2019 sur un intermédiaire de meublé donné
- **Données de localisation** : adresse du meublé et coordonnées géographiques associées (donnée facultative)
- **Autres types de données** : adresse universelle (« URL ») du meublé (donnée facultative), source(s) des données

### Base Légale

Le traitement est fondé sur les dispositions e du paragraphe 1 de [l’article 6](https://www.cnil.fr/fr/reglement-europeen-protection-donnees/chapitre2#Article6) du règlement général sur la protection des données (RGPD) relatives à l’exécution d’une mission d’intérêt public, dont est investie la DGE en vertu du décret n° 2009-37 du 12 janvier 2009 modifié relatif à la DGE et dont sont investies les communes en vertu de l’article L. 324-2-1 du code du
tourisme.

## Destinataires des données

### Catégories de destinataires

En fonction de leurs besoins respectifs, sont destinataires de tout ou partie des données :
- le personnel du PEReN, sous-traitant, chargé de l’administration et la gestion technique des outils numériques et du site web.
- les agents habilités des communes partenaires pour réaliser les contrôles au titre de l’article L. 324-2-1 du code du tourisme.

## Durée de conservation

Les données personnelles sont conservées :
- par le sous-traitant, six mois après la signature du contrat ;
- par les agents de la commune où est situé le meublé mis en location, un an après la transmission des données.

## Exercer vos droits

En application du RGPD et de la loi relative à l'informatique, aux fichiers et aux libertés, vous disposez d’un droit d’accès, de rectification, de limitation et d’opposition des données qui vous concernent.  

Pour toute information ou exercice de vos droits sur les traitements de données personnelles gérés de manière conjointe entre la DGE et les communes partenaires, vous pouvez nous contacter par voie postale ou électronique :
- **Pour la direction générale des entreprises et le PEReN** :  
Pôle d'Expertise de la Régulation Numérique  
120, rue de Bercy  
Bâtiment Necker  
Télédoc 767  
75572 PARIS CEDEX 12  
Ou par courrier électronique : api-meubles.dge[ @ ]finances.gouv.fr ;
- **Pour la ville de Bordeaux**:  
DPD/DPO Bordeaux Métropole  
Direction des affaires juridiques  
Esplanade Charles de Gaulle  
33045 Bordeaux Cedex.  
Ou par courrier électronique à : contact.cnil@bordeaux-metropole.fr
- **Pour la ville de Lyon**:  
François PALLIN  
Délégué à la Protection des Données  
Mairie de Lyon  
1, place de la Comédie  
69205 Lyon Cedex  
Ou via un formulaire : https://www.lyon.fr/plus-dinfos/protection-des-donnees
- **Pour la ville de Nice**:  
Karine CHOMAT  
Déléguée à la Protection des Données  
5 Rue de l’Hôtel de Ville  
06364 Nice Cedex 4  
Ou par courrier électronique à : dpd@nicecotedazur.org
- **Pour la ville de La Rochelle**:  
Délégué à la Protection des Données à caractère personnel de la Ville et de la Communauté d’agglomération de La Rochelle  
Communauté d’Agglomération de La Rochelle  
6 rue Saint-Michel  
CS 41287  
17086 La Rochelle Cedex 02  
Ou par courrier électronique à : dpd@ville-larochelle.fr
- **Pour la ville de Strasbourg**:  
M. Sélim-Alexandre ARRAD  
Ville de Strasbourg  
Délégué à la Protection des Données  
1 parc de l’Etoile  
67076 STRASBOURG CEDEX  
Ou par courrier électronique à : dpo@strasbourg.eu

En cas de difficultés dans l’exercice de vos droits, vous pouvez également contacter le délégué à la protection des données du MEFR :  
le-delegue-a-la-protection-des-donnees-personnelles[ @ ]finances.gouv.fr ;  
ou par courrier à l’adresse suivante :  
Le délégué à la protection des données des ministères économique et financier  
139, rue de Bercy  
75572 PARIS CEDEX 12

## Réclamation auprès de la CNIL

Si vous estimez, après avoir contacté le délégué à la protection des données compétent, que vos droits sur vos données ne sont pas respectés, vous pouvez [adresser une réclamation(plainte) à la CNIL](https://www.cnil.fr/fr/adresser-une-plainte).

## Ce site n’affiche pas de bannière de consentement aux cookies, pourquoi ?

Ce site ne dépose aucun cookie sur votre ordinateur lorsque vous le consultez. C’est pour cette  raison que nous n’affichons pas de bannière de consentement aux cookies, nous n’en avons simplement pas.