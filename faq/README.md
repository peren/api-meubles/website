<!--
SPDX-License-Identifier: MIT

This file is licensed under an MIT License.
Author: PEReN 2022
-->
# Foire aux Questions

:::details Je suis une commune ayant mis en place la procédure d’enregistrement et je suis intéressée par ce projet. Que puis-je faire ?
Vous êtes habilitée à demander une fois par an les états récapitulatifs de nuitées aux intermédiaires de meublés (cf. onglet cadre juridique).  
Si l’expérimentation est généralisée, vous pourrez bénéficier de l’interface. N’hésitez pas à vous signaler à nos équipes dès maintenant en remplissant le formulaire dédié (Lien vers la démarche simplifiée)*work in progress*. 
:::

:::details Je suis un intermédiaire de meublés, suis-je concerné par le projet ?
La participation au projet nécessite la signature d’une convention d’expérimentation entre la DGE, le PEReN, les communes et les intermédiaires.

Si vous n’êtes pas signataire d’une convention mais que vous êtes intéressé par l’expérimentation, n’hésitez pas à nous [contacter](/qui-sommes-nous/).
:::

:::details Je suis un loueur de meublés dans l’une des communes partenaires, comment exercer mes droits liés au RGPD ?
Contacter votre commune
*work in progress*
:::

## Aide pour la boite de dépôt

Aide à la prise en main de la boîte de dépôt et l'envoi de fichier:

:::details Première utilisation
**Prérequis**: Vous devez faire partie des entreprises expérimentatrices et vous munir de la clé d'API que vous aura fourni votre interlocuteur au PEReN  
Tout d'abord créez un fichier CSV correspondant à la [documentation](https://doc.api-meubles.peren.fr/).  
Rendez vous ensuite sur [ce lien](https://depot.meubles.app.peren.fr/).  
Vous pouvez sélectionner le fichier que vous voulez envoyer, copier-coller votre clé d'API dans le champs dédié et "Valider".
:::

En cas de message d'erreur sur la boîte de dépôt de l'API, vous aurez un code et un message d'erreur qui s'affichera. Cette section vise à vous aider à comprendre ces informations afin de pouvoir correctement envoyer votre fichier.
Pour rappel la documentation est disponible [ici](https://doc.api-meubles.peren.fr/)

:::details Code 403 - Invalid authentication
Ce message s'affiche en cas d'erreur avec la clé d'API nécessaire pour vous vous authentifier.
Cette clé vous a été normalement envoyé par le PEReN.
N'hésitez pas à la copier-coller pour éviter toutes fautes de frappe.
:::

:::details Code 400
Ce message s'affiche en cas de mauvaise requête. C'est un code d'erreur assez commun, il convient donc de se référer au message affiché.

* Missing required fields
Ce message indique qu'il manque une colonne obligatoire, vérifiez la 1ère ligne de votre fichier CSV contenant l'en-tête.
* No line in file
Ce message indique que le contenu du fichier n'a pas pu être lu au delà des en-têtes.
* NOK: Invalid XXXX field.
Ce message peut s'afficher pour n'importe quel champ, il signifie probablement que l'une de vos données ne respecte pas les critères d'acceptations (par exemple 5 chiffres pour un code postal ou simplement un code postal manquant)
Ce message d'erreur est normalement accompagné de l'identifiant du meublé, exemple: *"56AZYH68": "3-NOK: ad_cp"* pour vous aider à identifier la ligne problématique.
Quelques indications pouvant vous aider:
    * Les codes postaux doivent comporter exactement 5 chiffres.
    * Les dates doivent être au format JJ/MM/AAAA.
    * La colonne *est_residence principale* doit contenir *true* ou *false* en miniscule.
    * La colonne nb_nuits doit contenir un nombre.

:::

En cas d'erreurs différentes, notamment un code 500. Veuillez contacter votre interlocuteur PEReN et joindre ci-possible le fichier  provoquant l'erreur. Nous ferons de notre mieux pour vous accompagner. 



<!-- Si on veut du justifier, obliger de passer directement en HTML -->
<!-- <p style='text-align: justify;'>
<a href="https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000037639648">Do quis veniam</a> id ut ut non incididunt nostrud exercitation est et sunt enim est. Excepteur cillum esse laboris cupidatat velit adipisicing do nulla voluptate adipisicing esse elit. Anim exercitation tempor enim consequat. Velit nostrud eu ipsum commodo voluptate et enim ut amet eu do officia cupidatat. Nisi et enim exercitation laboris nostrud labore. Aute duis adipisicing Lorem ullamco voluptate ullamco minim ad laborum exercitation sunt enim officia eiusmod. Dolor reprehenderit occaecat Lorem duis.
</p> -->


