<!--
SPDX-License-Identifier: MIT

This file is licensed under an MIT License.
Author: PEReN 2022
-->
---
home: true
heroImage: /images/image-homepage-copyright-2.jpg
heroText: Fin de l'expérimentation API Meublés
tagline: L’expérimentation API meublés a été menée du 1er février au 4 octobre 2022. Elle consistait à tester en conditions réelles (auprès de quelques communes partenaires engagées) l'utilisation de la plateforme en ligne API Meublés visant à faciliter la transmission des données prévue à l'art. L324-2-1 du code du tourisme. L’équipe projet est actuellement en phase de réflexion sur la possibilité d’industrialiser la solution.
actionText: Présentation →
actionLink: /presentation/
---

<div class="features">
  <div class="feature" >
    <h2><b> Espace intermédiaires de meublés partenaires ?</b></h2>
    <p>Pour envoyer vos données aux communes, vous retrouverez le processus et la <a href="https://doc.api-meubles.peren.fr/">documentation</a> ici.</p>
    <p><a href="./intermediaire/">Plus d'informations</a></p>
  </div>
  <div class="feature" >
    <h2><b>Espace communes partenaires</b></h2>
    <p>Vous pouvez <a href="https://meubles.app.peren.fr/">accéder aux données</a> envoyées par les plateformes en vous connectant à votre espace.</p>
    <p><a href="./commune/">Plus d'informations</a></p>
  </div>
  <div class="feature" >
    <h2><b>Des questions ?</b></h2>
    <p>Vous pouvez consulter la section <a href="./faq/">FAQ</a>.</p>
    <p>Mais également <a href="./qui-sommes-nous/">nous contacter</a>.</p>
  </div>
</div>

