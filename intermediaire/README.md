<!--
SPDX-License-Identifier: MIT

This file is licensed under an MIT License.
Author: PEReN 2022
-->
# Espace intermédiaires de location de meublés partenaires
![Image API](./assets/image-api-8.jpg)  
<p style="font-size:x-small">© bsd555 / Getty Images.</p>



Si vous participez à l’expérimentation, la procédure pour envoyer vos données est la suivante :
-   Si vous ne l’avez pas déjà, demandez une clé d’authentification à l’adresse *api-meubles.dge@finances.gouv.fr*  en précisant les villes avec lesquelles vous avez signé une convention, le nom de votre entreprise, le nom et l’adresse mail du point de contact au sein de votre structure. 
-   Préparez vos données en vous appuyant sur la [documentation](https://doc.api-meubles.peren.fr/) suivante. En cas de question, vous pouvez contacter votre interlocuteur PEReN (qui vous aura transmis votre clé d'API)
-	Envoyez vos données sur l’interface soit en utilisant l’API, soit en passant par la boîte de dépôt (Lien *work in progress*).

## Envoi des données en utilisant l’API 

Pour utiliser l’API une fois votre clé d’authentification récupérée :
-	Envoyez vos données sur l’interface en respectant la structure des données et de la requête ([cf doc](https://doc.api-meubles.peren.fr/))
-	La réception d'un **code 200** indique que la transmission a bien été réalisée. La réception d’un autre code indique que l’envoi n’a pas fonctionné. Dans ce cas, vérifiez la structure de votre fichier et votre requête. 
-	Si vos difficultés persistent, consultez la [FAQ](/faq/) ou contactez votre interlocuteur PEReN en précisant le code d'erreur reçu et la méthode d'envoi employée.

## Envoi des données en utilisant la boîte de dépôt 

L’API est le moyen le plus efficace pour envoyer des données sur l’interface. Cependant, si vous ne disposez pas des ressources nécessaires pour vous en servir, une boîte de dépôt est proposée. 

Pour envoyer des données en passant par la boîte de dépôt :
-	Connectez-vous à : *work in progress* 
-	Entrez votre clé d’authentification et téléchargez votre tableau CSV (ce tableau doit respecter le format prédéfini, voir [doc](https://doc.api-meubles.peren.fr/))
-	Si votre envoi a fonctionné, vous recevez une confirmation.  Si au contraire le site vous retourne un message d’erreur, vérifiez la structure de votre fichier et votre clé d’authentification.
-	Si vos difficultés persistent,  consultez la [FAQ](/faq/) ou contactez votre interlocuteur PEReN en précisant le message d’erreur affiché. 
