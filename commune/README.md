<!--
SPDX-License-Identifier: MIT

This file is licensed under an MIT License.
Author: PEReN 2022
-->
# Espace communes partenaires
![Image Communes](./assets/image-commune-10.jpg)  
<p style="font-size:x-small">© invincible_bulldog / Getty Images.</p>

L’expérimentation est actuellement circonscrite aux communes ayant signé une convention d’expérimentation avec la DGE et les intermédiaires de meublés partenaires. 

## Vous êtes une commune partenaire ayant signé la convention d’expérimentation

Rendez-vous sur *https://meubles.app.peren.fr/*  pour accéder à vos données sur votre espace sécurisé dédié. Pour vous aider dans la prise en main de l’interface, un [tutoriel](https://marvelapp.com/prototype/ch9i99i) est mis en place à partir de données simulées. 
L’interface vous  permet : 
-	**d’accéder à des données harmonisées**  via la base d'adresse nationale ([BAN](https://adresse.data.gouv.fr/api-doc/adresse));
-	**de visualiser et d’analyser ces données** grâce à un tableau de bord présentant des statistiques et des tableaux filtrables ;
-	**d’exporter ces données** (en format csv, json, etc.) pour les exploiter sur d’autres outils. 

Les réunions du comité de travail et de suivi vous permettront d’exprimer vos éventuels besoins non couverts par l’interface et de préciser des fonctionnalités additionnelles qui pourraient être développées pour l’industrialisation. 
Pour toute autre question, n’hésitez pas à consulter la [FAQ](/faq/) ou à contacter vos interlocuteurs de la DGE. 

## Vous n’êtes pas une commune partenaire, mais vous avez mis en place la procédure d’enregistrement 

Vous êtes habilité à demander une fois par an les états récapitulatifs de nuitées aux intermédiaires de meublés (cf. onglet [Cadre juridique](/legislation/)). 
Si l’expérimentation est généralisée, vous pourrez bénéficier de l’interface. **N’hésitez pas à vous signaler à nos équipes dès maintenant** en remplissant le formulaire dédié (Lien vers la démarche simplifiée *work in progress*).  
