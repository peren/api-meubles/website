<!--
SPDX-License-Identifier: MIT

This file is licensed under an MIT License.
Author: PEReN 2022
-->
# Ouverture du code

L'expérimentation API-Meublés s'est terminée en octobre 2022 et les réflexions sur la manière d'industrialiser le projet ont débutées.  
Le code source de l'API est disponible [ici](https://code.peren.fr/peren/api-meubles)

# Contact

Se référer à [cette section](/qui-sommes-nous/#contact).
