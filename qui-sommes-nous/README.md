<!--
SPDX-License-Identifier: MIT

This file is licensed under an MIT License.
Author: PEReN 2022
-->
# Qui-sommes-nous ?

Ce projet est mené par la sous-direction du tourisme de la [DGE](https://www.entreprises.gouv.fr/fr) en partenariat avec le service à compétence nationale: "Pôle d'Expertise de la Régulation Numérique" (ou [PEReN](https://www.peren.gouv.fr/)).

## La direction générale des entreprises (DGE) 

Au service du ministre de l’Économie, des Finances et de la Relance la DGE conçoit et met en œuvre les politiques publiques concourant au développement des entreprises. Son action est au cœur des chantiers du gouvernement pour la transformation économique du pays. Elle porte des missions à la fois sectorielles (politique industrielle, régulation du numérique et déploiement des infrastructures, politiques de soutien à l’artisanat, au commerce, aux services et au tourisme), transverses (simplification règlementaire, politique d’innovation) et relatives à la transformation numérique et écologique de l’économie.

Pour en savoir plus : [https://www.entreprises.gouv.fr/ ](https://www.peren.gouv.fr/equipe/)

## Le pôle d’expertise de la régulation numérique (PEReN)

Le PEReN est un service à compétence nationale, rattaché au Directeur Général des Entreprises pour sa gestion administrative et financière, et placé sous l’autorité conjointe des ministres chargés de l’économie, de la culture et du numérique, dont les missions et l’organisation sont fixées par le décret du 31 août 2020.

Pour en savoir plus : [https://www.peren.gouv.fr/equipe/](https://www.peren.gouv.fr/equipe/)

# Contact

En cas de question, vous pouvez contacter : *api-meubles.dge@finances.gouv.fr*  
Si vous vous participez déjà à l’expérimentation, veuillez contacter directement vos interlocuteurs DGE ou PEREN. 

