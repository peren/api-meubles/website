<!--
SPDX-License-Identifier: MIT

This file is licensed under an MIT License.
Author: PEReN 2022
-->
# Présentation de l'expérimentation

Afin d’améliorer l’application des dispositions de [l’article L. 324-2-1](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000042116125/2020-01-01) du code du tourisme  et d’automatiser autant que possible la transmission des états récapitulatifs de nuitées, la direction générale des entreprises (DGE) met en œuvre l’expérimentation, décidée par le Gouvernement, d’une interface visant à faciliter les échanges de données entre les intermédiaires de location de meublés de tourisme et les communes.  

Les objectifs visés par la mise en place de cette interface sont les suivants :
-	harmoniser et simplifier les échanges de données entre communes et intermédiaires de location ;
-	automatiser les échanges pour accélérer et faciliter la mise à disposition de l’information ; 
-	centraliser  l’effort de correction et de réconciliation de données pour les communes.

## Informations échangées

[L’article L. 324-2-1](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000042116125/2020-01-01) du code du tourisme habilite les communes ayant mis en place le numéro d’enregistrement à demander à un intermédiaire de location le décompte du nombre de jours au cours desquels ce meublé de tourisme a fait l'objet d'une location par son intermédiaire, sur l'année en cours et l'année précédant la demande. 

![Image décrivant les données qui se composent de: l'adresse, le numéro de déclaration, le nombre de jours de location sur l'année en cours et précédente, du nom du loueur et s'il s'agit d'une résidence principale](./assets/presentation1.png)

Ce sont ces données qui peuvent être transmises via l’interface. Leur format est fixé par un arrêté du [31 octobre 2019](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000039309243/2019-11-06/) (modifié par un [arrêté du 14 décembre 2020](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000042665804)).

## Fonctionnement de l’interface

Grâce à l’interface, conçue pour être flexible, chaque partie prenante n’a qu’un **seul point de contact** et dispose d’un **format d’échange sécurisé et adapté** à ses besoins et ses contraintes.

![Image décrivant le fonctionnement du projet. Premièrement les intermédiaires envoient les données à l'interface de manière sécurisée et structurée. Deuxièmement, l'interface retraite et harmonise les données. Troisièmement, les collectivités peuvent se connecter à un outil pour visualiser et exporter les données.](./assets/presentation2.png)

## Etapes du projet

Cinq territoires pilotes participent à la phase d’expérimentation, qui permet de tester le parcours utilisateur de bout en bout avec les parties prenantes pour s’assurer du bon fonctionnement technique de l’interface et pour vérifier son adéquation aux besoins des différentes parties. Ces travaux visent à évaluer la pertinence d’une généralisation de l’interface à l’ensemble des intermédiaires de meublés et des communes territoriales ayant mis en place l’enregistrement et à préciser les modalités de la mise en production, notamment les prérequis techniques et les coûts.

![Image décrivant les étapes du projet. Premièrement: préfiguration de l'expérimentation, mise en place du cadre expérimental, développement du produit minimum viable, tests techniques. Deuxièmement: Expérimentation avec  territoires pilotes, groupes de travail  avec l'ensemble des parties prenantes, evaluation juridique et technique, définitions de scénarios d'industrialisation. Enfin, si la généralisation du projet est validée, le projet sera mis en production pour l'ensemble des communes habilitées à demander les récapitulatifs de nuitées.](./assets/presentation3.png)
