/*
 * SPDX-License-Identifier: MIT
 *
 * This file is licensed under an MIT License.
 * Author: PEReN 2022
 */
const {getSidebar} = require('vuepress-theme-gouv-fr/sidebar.js')

module.exports = {
    title: 'API Meublés',
    base: (process.env.DEPLOYER_GIT_VERSION === 'stable' ? '/' : '/api-meubles/website/'),  // Pour faire fonctionner l'option "pages" de gitlab - ATTENTION change aussi l'adresse en localhost
    description: 'Pour vous accompagner dans l\'utilisation de la plateforme en ligne API Meublés et l\'exploitation de ses données',
    theme: 'vuepress-theme-gouv-fr',
    themeConfig: {
        logo: '/images/logo-marianne.svg',
        search: false,
        sidebar: getSidebar(),
        sidebarDepth: 1,

        nav: [
            { text: 'Accueil', link: '/'},
            { text: 'Présentation', link: '/presentation/'},
            { text: 'Cadre juridique', link: '/legislation/' },
            { text: 'Qui-sommes-nous ?', link: '/qui-sommes-nous/'},
            { text: 'Ouverture', link: '/ouverture/'},
            { text: 'FAQ', link: '/faq/'},
            { text: 'Mentions légales', link: '/mentions/'}
          ],
    }
}
