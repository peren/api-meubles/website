<!--
SPDX-License-Identifier: MIT

This file is licensed under an MIT License.
Author: PEReN 2022
-->
# Site de présentation du projet API-Meublés

Ce site web est hébergé par le PEReN et disponible via ce [lien](https://meubles.peren.fr/).

Il a vocation à présenter succinctement le projet API-Meublés et son cadre juridique.
