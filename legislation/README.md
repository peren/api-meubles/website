<!--
SPDX-License-Identifier: MIT

This file is licensed under an MIT License.
Author: PEReN 2022
-->
# Cadre Juridique

Vous trouverez ci-dessous les informations concernant le cadre juridique dans lequel le projet est mené.

## Définition du meublé de tourisme ?

Les meublés de tourisme sont définis par [l’article L. 324-1-1 du code du tourisme](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000042070525/). Il s’agit des villas, appartements ou studios meublés, à l’usage exclusif du locataire, offerts à la location à une clientèle de passage qui n’y élit pas domicile et qui y effectue un séjour caractérisé par une location à la journée, à la semaine ou au mois.  

Les meublés de tourisme peuvent faire l’objet d’un classement ou d’une labellisation , mais cela reste facultatif. 

## La procédure de changement d’usage

La **procédure de changement d’usage** est un dispositif ancien qui vise à lutter contre la pénurie de logements. **Sa mise en œuvre a pour effet de soumettre à autorisation la transformation de tout logement en un local à autre usage (notamment, mais pas uniquement, en meublé de tourisme).**

Aujourd’hui, cette procédure permet en particulier **aux communes qui connaissent des tensions importantes sur le marché du logement** de prévenir leur aggravation.

Cette procédure interdit la location sans avoir obtenu l’autorisation de changement d’usage, d’une résidence secondaire, quelle que soit la durée de cette location.

La délivrance d’une autorisation de changement d’usage **peut être soumise à compensation**.

**Dans les communes où la mise en œuvre du changement d’usage est facultative, celle-ci doit être dûment justifiée**. La jurisprudence française et européenne a confirmé que la procédure de changement d’usage répondait bien au motif impérieux d’intérêt général qu’est la protection du logement à destination de la population permanente. Cependant, **il appartient à chaque commune de démontrer qu’elle subit bien des tensions de nature à justifier une telle intervention**. Il apparaît à ce titre utile de pouvoir s’appuyer sur des études précises permettant de démontrer et de caractériser la tension locale sur le marché du logement.

:::tip Information
Les [articles L. 631-7 à 631-9](https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000006074096/LEGISCTA000019286496/#LEGISCTA000019286917) et [L. 651-2](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000038791019/) du code de la construction et de l’habitation (CCH) pour la réglementation du changement d’usage.
:::

## La mise en œuvre du numéro d’enregistrement

Le numéro d’enregistrement peut être mis en œuvre dans toute commune qui applique la procédure de changement d’usage, par simple délibération du conseil municipal.

Concrètement, la commune doit alors mettre en place un téléservice (site web), qui permet aux loueurs de se déclarer, et d’obtenir immédiatement et automatiquement un numéro d’enregistrement standardisé, à treize caractères.

Le contenu de la déclaration, ainsi que le format du numéro d’enregistrement, sont précisés par le II de [l’article D. 324-1-1 du code du tourisme](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000039475092/). Ainsi, la déclaration doit comporter notamment l’identité et l’adresse du loueur, l’adresse et les caractéristiques du local loué (notamment s’il s’agit ou non d’une résidence principale).

L’obtention d’un numéro d’enregistrement ne vaut pas autorisation de changement d’usage ; il s’agit de deux démarches indépendantes dont les finalités sont différentes.

## Les modifications législatives et réglementaires apportées par la loi ELAN

[L’article 145](https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000037639648) de la loi n° 2018-1021 du 23 novembre 2018 portant évolution du logement, de l'aménagement et du numérique, dite loi ELAN, est venu compléter le cadre législatif de régulation du secteur des locations en meublés de tourisme. Il a modifié [l’article L. 324-1-1](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000042070525/2020-01-01) du code du tourisme relatif aux obligations des loueurs en meublés, ainsi que [l’article L. 324-2-1](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000042116125/2020-01-01)  du même code relatif aux obligations des intermédiaires de location de meublés de tourisme. La loi ELAN introduit, pour les communes ayant mis en place le numéro d’enregistrement, la limite des 120 jours de location d’un meublé tourisme s’il s’agit d’une résidence principale. Elle habilite également les communes concernées à demander à un intermédiaire de location le décompte du nombre de jours au cours desquels ce meublé de tourisme a fait l'objet d'une location par son intermédiaire, sur l'année en cours et l'année précédant la demande. L’article 55 de la [loi n° 2019-1461 du 27 décembre 2019](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000039681877/) relative à l’engagement dans la vie locale et à la proximité de l'action publique a complété la liste d’informations que peut demander la commune

::: tip Information
- [Arrêté du 31 octobre 2019](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000039309243/) précisant le format des tableaux relatifs aux transmissions d'informations prévues par les articles R. 324-2 et R. 324-3 du code du tourisme.
- [Arrêté du 14 décembre 2020](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000042665804) modifiant l'arrêté du 31 octobre 2019 précisant le format des tableaux relatifs aux transmissions d'informations prévues par les articles R. 324-2 et R. 324-3 du code du tourisme.
::: 

## Transmission des données entre les intermédiaires de meublés et les communes ?

**1) En quoi consiste la demande de données au titre de la loi ELAN ?**

Les communes qui appliquent la procédure de changement d’usage et le numéro d’enregistrement peuvent, une fois par an, demander aux plateformes offrant à la location des biens situés sur leur territoire, la transmission d’un état récapitulatif du nombre de nuitées de location de chaque local pendant l’année en cours et l’année précédente. Cet état précise également le nom du loueur, si le local constitue ou non sa résidence principale, ainsi que le numéro d’enregistrement et l’adresse précise du local.

::: tip Information
Cette demande de données est distincte des états que les plateformes doivent transmettre, deux fois par an, aux communes pour le compte desquelles elles collectent la taxe de séjour.
:::

**2) Comment faire une demande de données au titre de la loi ELAN ?**

La demande concerne les données de l’année en cours, ainsi que celles de l’année précédente. Elle doit être adressée par voie électronique à chaque plateforme pour laquelle la commune souhaite obtenir un récapitulatif.

La réponse de la plateforme doit intervenir sous un mois, dans un format défini par [arrêté](https://www.legifrance.gouv.fr/loda/id/LEGITEXT000039313169/2019-11-06/#LEGITEXT000039313169), et ce afin que les fichiers transmis par plusieurs plateformes soient aisément comparables.
